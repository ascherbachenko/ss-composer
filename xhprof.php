<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once "/var/www/html/xhprof/xhprof_lib/utils/xhprof_lib.php";
include_once "/var/www/html/xhprof/xhprof_lib/utils/xhprof_runs.php";

$xhprof_runs = new XHProfRuns_Default();
xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

function get_big_array()
{
    return range(1, 1000000);
}

function iterate_through_array($array)
{
    foreach ($array as $key => $value) {
        if (is_string($value)) {
            break;
        }
    }

    return $array;
}

$big_array = get_big_array();
iterate_through_array($big_array);

$xhprof_data = xhprof_disable();
$run_id = $xhprof_runs->save_run($xhprof_data, "test");

echo "Run id: {$run_id}";

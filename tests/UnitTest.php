<?php

use PHPUnit\Framework\TestCase;
use Unit\Unit;

class UnitTest extends TestCase
{
    private $unit = null;

    public function __construct()
    {
        $this->unit = new Unit('meters');
    }

    public function testGetUnit()
    {
        $this->assertEquals($this->unit->getUnit(), 'meters per second');
    }

    public function testGetArray()
    {
        $this->unit->setArray(1, 2, 3);
        $this->assertEquals($this->unit->getArray(), array(1, 2, 3));
    }

    public function testCheckArrayLength()
    {
        $this->assertFalse(count($this->unit->getArray()) == 3);
    }
}

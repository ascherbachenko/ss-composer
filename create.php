<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Query\Builder;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'composer',
    'username'  => 'root',
    'password'  => 'root',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$connection = $capsule->getConnection();

$capsule->setAsGlobal();

if (!Capsule::schema()->hasTable('users')) {
    Capsule::schema()->create('users', function($table)
    {
        $table->increments('id');
        $table->string('email')->unique();
        $table->string('extra');
        $table->timestamps();
    });

    $builder = new Builder($connection);
    $builder ->from('users');

    $builder->insert(array(
        array(
            'email' => 'test@mail.ru',
            'extra' => 'some extra data',
            'created_at' => gmdate('Y-m-d H:i:s'),
            'updated_at' => gmdate('Y-m-d H:i:s'),
        ),
        array(
            'email' => 'test1@mail.ru',
            'extra' => 'some extra data',
            'created_at' => gmdate('Y-m-d H:i:s'),
            'updated_at' => gmdate('Y-m-d H:i:s'),
        ),
        array(
            'email' => 'test2@mail.ru',
            'extra' => 'some extra data',
            'created_at' => gmdate('Y-m-d H:i:s'),
            'updated_at' => gmdate('Y-m-d H:i:s'),
        ),
        array(
            'email' => 'test3@mail.ru',
            'extra' => 'some extra data',
            'created_at' => gmdate('Y-m-d H:i:s'),
            'updated_at' => gmdate('Y-m-d H:i:s'),
        ),
    ));
}


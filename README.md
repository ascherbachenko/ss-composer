# README #

Main info - https://getcomposer.org/doc/00-intro.md

### Installation (for local usage) ###

* php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
* php -r "if (hash_file('SHA384', 'composer-setup.php') === 'c32408bcd017c577ce80605420e5987ce947a5609e8443dd72cd3867cc3a0cf442e5bf4edddbcbe72246a953a6c48e21') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
* php composer-setup.php
* php -r "unlink('composer-setup.php');"

### Versioning ###
* https://getcomposer.org/doc/articles/versions.md - full version
* Exact
    * 1.0.2
* Range
    * operators - ```>, >=, <, <=, !=```
    * AND ```a space ( ) or comma (,)```
    * OR ```||```
    * Examples ```>=1.0``` ```>=1.0 <2.0``` ```>=1.0 <1.1 || >=1.2```
 
* Wildcard
    * 1.0.*

### Installing Dependencies ###
* php composer.phar install

### Main repository ###
* https://packagist.org/

### Generating autoloader ###
* php composer.phar dump-autoload

### Removing package (with dependencies)
* php composer.phar remove sylius/mailer
* php composer.phar update

### Phinx ###
* php vendor/bin/phinx create TestMigration
* php vendor/bin/phinx migrate -e development
* php vendor/bin/phinx rollback -e development

### PHPUnit ###
* php composer.phar require phpunit/phpunit:5.7.*
* vendor/bin/phpunit --verbose --bootstrap vendor/autoload.php tests/

### Profiler ###
* http://ruhighload.com/index.php/2009/08/21/xhprof-%D0%BF%D1%80%D0%BE%D1%84%D0%B8%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-php-%D0%BE%D1%82-facebook/
* https://andymarrel.eu/programming/xhprof-php-7
* http://www.kam24.ru/xhprof/docs/
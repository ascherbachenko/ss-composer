<?php

namespace Unit;

class Unit
{
    private $unit = null;
    private $array = null;

    public function __construct($unit = null)
    {
        $unit = $unit ? $unit : 'meters per second';
        $this->unit = $unit;
    }

    public function setUnit($unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setArray()
    {
        $this->array = func_get_args();

        return $this;
    }

    public function getArray()
    {
        return $this->array;
    }
}
